# Route Decorator

## v1.3.0

Adds `compileOrNull` method.

## v1.2.0

Adds support for query strings as a second parameter.
Query parameters are matched and parsed *regardless* of order.
See documentation for more info.

```typescript
class Routes {
	@route('/', 'page=:page?&tag=:tags*')
	home({ page, tags }: { page?: string, tags?: string[] }) {
		console.log(page, tags);
	}
}

const router = route.getRouter(new Routes());

router.route('/', 'tag=red&page=4&tag=yellow');
// logs '4' and ['red', 'yellow']
```

### v.1.2.1

Fixes support for `undefined` and `null` as query parameter values,
counting them as absent instead of stringifying them.

## v1.1.0

Adds the ability to clone an existing instance of the decorator
by passing it to the router:

```typescript
const route = new RouteMap<Routes>();

class Routes {
	@route('/')
	home(){ return 'Home' }
}

const authRoute = new RouteMap<RoutesWithAuth>(route);

class RoutesWithAuth extends Routes {
	@authRoute('/auth')
	auth() { return 'Auth endpoint'; }
}
```

## v1.0.0

Initial code - majority of features were added at version 1
