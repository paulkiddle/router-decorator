import { PathAndQueryExpression } from './path.js';

type RouteReturnType<T> = T extends (...args: any) => infer R ? R : never;

type RouterRecord<Router> = Record<keyof Router, Router[keyof Router]>;

type RouteKey<Router extends RouterRecord<Router>, Key extends keyof Router = keyof Router> = Router[Key] extends (...a: any) => unknown ? Key : never;

interface RouterDecorator<T> {
	/**
	Return a decorator that maps a path template to this route
	@param path The path template that maps to this route
	 */
	(path: string, query?: string): T
}

interface RouterConstructor {
	new<Router extends RouterRecord<Router>, Key extends keyof Router = keyof Router>(...args: Parameters<typeof RouteDecoratorConstructor<Router, Key>>):
		RouterDecorator<<K extends Key>(fn: unknown, ctx: { name: K; kind: 'method'; private: false }) => void>
}

function RouteDecoratorConstructor<Router extends RouterRecord<Router>, Key extends keyof Router = keyof Router>(routes: Map<Key, PathAndQueryExpression[]>) {
	return Object.setPrototypeOf(function route(path: string, query?: string) {
		const expr = new PathAndQueryExpression(path, query);

		return function <K extends Key>(fn: unknown, ctx: { name: K; kind: 'method'; private: false }) {
			const paths = routes.get(ctx.name);
			if (paths) {
				// Use unshitft because decorators are applied in the reverse order
				// to how they appear in the code.
				paths.unshift(expr);
			} else {
				routes.set(ctx.name, [expr]);
			}
		};
	}, new.target.prototype);
}

RouteDecoratorConstructor.prototype = Object.create(Function.prototype);

const RouteDecorator = RouteDecoratorConstructor as unknown as RouterConstructor;

type RouteParams = { [s: string]: string|string[] };

/**
 * Router decorator and path mapper
 */
export class BaseRouteMap<Routes extends RouterRecord<Routes>, Keys extends keyof Routes = keyof Routes> extends RouteDecorator<Routes, Keys> {
	#routeMap: Map<Keys, PathAndQueryExpression[]>;

	constructor(clone?: BaseRouteMap<Routes, Keys>){
		const routeMap = new Map<Keys, PathAndQueryExpression[]>(clone && clone.#routeMap);

		super(routeMap);

		this.#routeMap = routeMap;
	}

	/**
	 * Generate a path string for a named route with given parameters
	 * @param name The name of the route to create a path for
	 * @param data The dict of parameters to add to the path (or undefined)
	 * @returns The compiled path string
	 */
	compile<K extends Keys>(name: RouteKey<Routes, K>, ...data: Parameters<Routes[K]>) {
		return this.#compile(name, data);
	}

	#compile(name: any, data: unknown[]) {
		const r = this.#routeMap.get(name)
		if(!r) {
			throw new Error(`No route path registered for ${String(name)}. Did you forget to add the decorator?`);
		}
		const d = (0 in data ? data[0] as RouteParams : undefined);

		let selected: PathAndQueryExpression|null = null;
		let size = -1;

		for(const tpl of r) {
			if(tpl.tokens.every(t=>t.optional||(d && (t.name in d)))) {
				if(tpl.size > size) {
					size = tpl.size;
					selected = tpl;
				}
			}
		}

		if(!selected) {
			throw new Error(`Incorrect parameters for route ${String(name)}`);
		}

		return selected.compile(d);
	};

	/**
	 * Generate a path string for a named route with given parameters,
	 * or return null if the named path does not exist.
	 * @param name The name of the route to create a path for
	 * @param data The dict of parameters to add to the path (or undefined)
	 * @returns The compiled path string, or null
	 */
	compileOrNull(name: any, data?: RouteParams) {
		if(this.#routeMap.has(name)) {
			return this.#compile(name, [data])
		}
		return null;
	}

	/**
	 * Iterate over all of the routes that match a given path
	 * @param pathname The path to match against
	 * @yields A function that accepts a route class instance and calls the route with the parsed parameters
	 */
	*matchAll(pathname: string, query?: string|URLSearchParams): Generator<((routes: Routes) => RouteReturnType<Routes[Keys]>)> {
		for (const [name, paths] of this.#routeMap) {
			for(const expr of paths) {
				const matched = expr.match(pathname, query);

				if (matched) {
					yield (routes: Routes) => routes[name](matched);
				}
			}
		}
	};

	/**
	 * Find a route for the given pathname
	 * @param pathname The path to match against
	 * @returns A function that accepts a routes class instance and calls the route with the parsed parameters, or null if no routes match
	 */
	match (pathname: string, query?: string|URLSearchParams) {
		for (const matched of this.matchAll(pathname, query)) {
			return matched;
		}

		return null;
	};
}

export default class RouteMap<Routes extends RouterRecord<Routes>, Keys extends keyof Routes = keyof Routes> extends BaseRouteMap<Routes, Keys> {
	/**
	 * Create and return a router class for a given routes class instance
	 * @param routes An instance of the routes class
	 * @returns An instance of the Router class
	 */
	getRouter(routes: Routes) {
		return new Router(this, routes);
	}
}

export class RouteNotFoundError extends TypeError {
	code = 'RouteNotFound' as const
}

/**
 * A router that can run routes for a given routes class
 */
export class Router<T> {
	#router;
	#pathmap;
	
	constructor(pathmap: RouteMap<T>, routes: T) {
		this.#router = routes;
		this.#pathmap = pathmap
	}

	/**
	 * Iterate over all routes and yield the value of each when executed
	 * @param path Path to match against
	 */
	*routeAll(path: string, query?: string|URLSearchParams) {
		for(const matched of this.#pathmap.matchAll(path, query)) {
			yield matched(this.#router);
		}
	}

	/**
	 * Check if the given path matches any decorator paths
	 * @param path Path to match against
	 * @returns boolean
	 */
	has(path: string, query?: string|URLSearchParams) {
		return !!this.#pathmap.match(path, query);
	}

	/**
	 * Execute the first found route or return a fallback value
	 * @param path 
	 * @param fallback 
	 * @returns 
	 */
	routeOrFallback<D>(path: string, fallback: D) {
		return this.#routeOrFallback(path, undefined, fallback);
	}

	#routeOrFallback<D>(path: string, query: string|undefined|URLSearchParams, fallback: D) {
		const matched = this.#pathmap.match(path, query);

		if(matched) {
			return matched(this.#router);
		}

		return fallback;
	}

	/**
	 * Execute the first found route or return null
	 * @param path 
	 * @returns 
	 */
	routeOrNull(path: string, query?: string|URLSearchParams) {
		return this.#routeOrFallback(path, query, null);
	}

	/**
	 * Execute the first found route or throw an error
	 * @param path 
	 * @returns 
	 */
	route(path: string, query?: string|URLSearchParams) {
		const matched = this.#pathmap.match(path, query);

		if(matched) {
			return matched(this.#router);
		}

		// Did you forget to decorate your route?
		const q = query ? ('?' + query) : ''
		throw new RouteNotFoundError('Route not found for path ' + path + q);
	}
}
