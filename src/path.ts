import { MatchFunction, PathFunction, compile, match, parse } from "path-to-regexp";
import QueryMatcher from './query/query.js';

type Dict = Record<string, string|string[]>

type Token = {
	name: string|number,
	optional: boolean
}

export class PathAndQueryExpression {
	#path: string
	#query?: QueryMatcher
	#tokens?: Token[]
	#compiler?: PathFunction
	#matcher?: MatchFunction

	constructor(path: string, query?: string) {
		this.#path = path;
		this.#query = query ? new QueryMatcher(query) : undefined;
	}

	*#getTokens(){
		for(const t of parse(this.#path)) {
			if(typeof t === 'object') {
				yield {
					name: t.name,
					optional: (t.modifier === '?' || t.modifier === '*')
				}
			}
		};
	}

	get tokens() {
		const t = this.#getTokens();
		return this.#tokens ??= this.#query ? [...t, ...this.#query.tokens] : [...t];
	}

	get size() {
		return this.tokens.length;
	}

	get #compilePath(){
		return this.#compiler ??= compile(this.#path, { encode: encodeURIComponent });
	}

	compile(d?: Dict) {
		const path = this.#compilePath(d);

		if(this.#query) {
			if(d) {
				for(const { name: key } of this.#getTokens()) {
					const prop = d[key];
					if(Array.isArray(prop) && prop.length > 1) {
						d[key] = prop.slice(1);
					} else {
						delete d[key];
					}
				}
			}

			const compiled = this.#query.compile(d);

			return compiled ? `${path}?${compiled}` : path;
		}

		return path;
	}

	match(path: string, query?: string|URLSearchParams) {
		const matcher = this.#matcher ??= match(this.#path, { decode: decodeURIComponent });
		const matched = matcher(path);

		if(matched && this.#query) {
			return this.#query.parse(query ?? '', matched.params as Dict);
		}
		return matched ? matched.params : null;
	}
}
