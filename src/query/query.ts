import CherryPicker from './cherry-picker.js';

export type Rest = [string, string][];

type V = string|string[]|Rest|undefined;
type Options = {
	[s: string]: V
};


const operators = {
	'': {
		optional: false,
		multiple: false
	},
	'?': {
		optional: true,
		multiple: false
	},
	'+': {
		optional: false,
		multiple: true
	},
	'*': {
		optional: true,
		multiple: true
	},
}

type Token = {
	type: 'rest',
	name: string
} | {
	type: 'literal',
	param: string,
	value: string
} | Variable;

type Variable = {
	type: 'variable',
	multiple: boolean,
	param: string,
	optional: boolean,
	name: string
}

const assertTokenOptional = (token: Variable) => {
	if(!token.optional) {
		throw new Error(token.name + ' not provided.')
	}
}

export default class SearchParamsExpression<T extends Options=Options> {
	#tokens: Token[]

	constructor(input: string) {
		this.#tokens = input.split('&').map(
			segment => {
				const rest = segment.match(/^:([a-z0-9%]+)\.\.\.$/i);
				if(rest) {
					return {
						type: 'rest' as const,
						name: rest[1]
					}
				}

				const [param, spec] = segment.split('=');
				const m = spec.match(/^:([a-z0-9%]+)([?+*]?|\.\.\.)$/i);

				if(!m) {
					return {
						type: 'literal' as const,
						param,
						value: spec
					}
				}

				if(m[2] === '...') {
					throw new Error(`The rest segment must not have a query key; change \`${param}=${spec}\` to just \`${spec}\``)
				}

				
				return {
					type: 'variable' as const,
					name: m[1],
					param,
					...operators[m[2] as keyof typeof operators]
				}
			}
		);

	}

	parse(str: string|URLSearchParams, entries: Options) {
		const q = new CherryPicker(Array.from(new URLSearchParams(str)));

		for(const token of this.#tokens) {
			if(token.type === 'literal' && !q.pickEntry(token.param, token.value)) {
				return null;
			}

			if(token.type === 'variable') {
				if(token.multiple) {
					const value = q.pickMultiple(token.param);
					if(!token.optional && value.length < 1) {
						return null;
					}
					entries[token.name] = value;
				} else {
					const value = q.pick(token.param);
					if(value == undefined) {
						if(!token.optional) {
							return null;
						}
					} else {
						entries[token.name] = value;
					}
				}
			}

			if(token.type === 'rest') {
				entries[token.name] = Array.from(q.entries)
			}
		}
		return entries as T;
	}

	get tokens() {
		return this.#tokens.filter((t): t is Variable=>t.type==='variable');
	}

	compile(entries?: T) {
		const q = new URLSearchParams();

		for(const token of this.#tokens) {
			switch(token.type) {
				case 'literal':
					q.append(token.param, token.value);
					break;
				case 'variable':
					if(token.multiple) {
						const entriesM = (entries as Record<string, string[]>);
						const values = entriesM[token.name];
						if(values.length > 0) {
							for(const v of values) {
								q.append(token.param, v as string);
							}
						} else {
							assertTokenOptional(token);
						}
					} else {
						const entriesS = (entries as Record<string, string>)
						if(token.name in entriesS && entriesS[token.name] != undefined) {
							q.append(token.param, entriesS[token.name]);
						} else {
							assertTokenOptional(token);
						}
					}
					break;
				case 'rest':
					const rest = (entries as Record<string, [string, string][]>);
					if(rest[token.name]) {
						for(const entry of rest[token.name]) {
							q.append(...entry)
						}
					}
					break;
			}
		}
		return q.toString();
	}
}
