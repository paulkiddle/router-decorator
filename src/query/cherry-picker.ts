type Entries = [string, string][];

export default class CherryPicker {
	#entries: Entries;

	constructor(entries: Entries) {
		this.#entries = entries;
	}

	*pickAll(name: string) {
		const items = this.#entries;

		let ix;
		while(ix = items.findIndex(([n]) => n === name), ix > -1) {
			yield items.splice(ix, 1)[0][1];
		}
	}

	pickMultiple(name: string) {
		return Array.from(this.pickAll(name));
	}

	pick(name: string) {
		for(const val of this.pickAll(name)) {
			return val;
		}
	}

	pickEntry(name: string, value: string) {
		const ix = this.#entries.findIndex(e => e[0] === name && e[1] === value);

		if(ix > -1) {
			this.#entries.splice(ix, 1);
			return true;
		}

		return false;
	}

	*[Symbol.iterator]() {
		yield* this.#entries;
	}

	get entries() {
		return [...this];
	}
}
