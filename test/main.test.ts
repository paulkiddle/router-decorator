import RouteMap from "../src/main.js"; 

describe('Test', () => {
	const assert = (x: unknown, m?: string) => expect(x).toBeTruthy();
	assert.deepEqual = (x: unknown, y:unknown) => expect(x).toEqual(y);
	assert.equal = (x: unknown, y: unknown) => expect(x).toBe(y);
	assert.throws = (x: unknown) => expect(x).toThrow();

const route = new RouteMap<TestRoutes>;

class TestRoutes {
	@route('/')
	home(){
		return 'Home page'
	}


	@route('/user/:userId')
	@route('/default-user')
	@route('/user','id=:userId')
	user({ userId }: { userId: string } = { userId: 'default' }) {
		return 'User ID ' + userId
	}

	@route('/query', 'many=:d+&some=:c*&optional=:b?&required=:a&fixed=0&:e...')
	q<T>(options: T) {
		return options
	}

	@route('/q2/:o+', 'a=:a?')
	q2<T>(options: T) {
		return options
	}

	@route('/q3', 'a=:a&b=:b+')
	q3<T>(options: T) {
		return options
	}
}

const router = route.getRouter(new TestRoutes);
test('path', ()=>{

assert(router.has('/'));

assert(!router.has('/not'));

assert.deepEqual(Array.from(router.routeAll('/')), ['Home page']);

assert.equal(router.routeOrNull('/user/fred'), 'User ID fred');

assert.equal(router.routeOrNull('/not'), null);
assert.equal(router.routeOrFallback('/not', 'fell'), 'fell');

assert.equal(router.route('/user/123'), 'User ID 123');

assert.throws(() => router.route('/nope'));
assert.throws(() => router.route('/nope','no'));

assert.equal(route.compile('home'), '/');

assert.equal(route.compile('user', { userId: '2' }), '/user/2');
assert.equal(route.compile('user'), '/default-user');

const cloned = new RouteMap<ExtendedRoutes>(route);

class ExtendedRoutes extends TestRoutes {
	@cloned('/home2/:str?')
	home(params?: { str: string }){
		return params?.str ?? super.home()
	}

	@cloned('/extra')
	extra(){
		return 'extra route'
	}
}

const extendedRouter = cloned.getRouter(new ExtendedRoutes);

assert(extendedRouter.route('/home2/str'), 'str');
assert(extendedRouter.route('/home2'), 'Home page');

assert(extendedRouter.route('/extra'), 'Extra route');
});
	test('Query', () => {
		expect(route.compile('q', { a: 'a', c: ['c', 'd'], b: 'b', d: ['d'], e: [['e', 'e']] }))
			.toEqual('/query?many=d&some=c&some=d&optional=b&required=a&fixed=0&e=e');

		const r = router.route('/query', 'many=v1&some=v2&optional=v3&required=v4&fixed=0&rest=val');

		expect(r).toEqual({
			d: ['v1'],
			c: ['v2'],
			b: 'v3',
			a: 'v4',
			e: [['rest', 'val']]
		})

		expect(router.route('/q2/o', 'a=a')).toEqual({ o:['o'], a:'a' })
		expect(route.compile('q2', { o:'o', a:'a' })).toEqual('/q2/o?a=a')
		expect(route.compile('q2', { o:['o', 'o2'], a:'a' })).toEqual('/q2/o/o2?a=a')
		expect(route.compile('q2', { o:'o' })).toEqual('/q2/o')
		expect(route.compile('q2', { o:'o', a: undefined })).toEqual('/q2/o')
		expect(router.route('/q2/o')).toEqual({ o: ['o'] })

		expect(() => route('/', 'a=:b...')).toThrow()

		expect(router.routeOrNull('/query', 'many=a&required=a')).toBeNull()

		expect(router.routeOrNull('/query', 'required=a&fixed=0')).toBeNull()
		expect(router.routeOrNull('/query', 'many=a&fixed=0')).toBeNull()

		expect(() => route.compile('q', { a: 'a' })).toThrow();
		expect(() => route.compile('q', { d: ['d'] })).toThrow();

		expect(() => route.compile('q3', { a: 'a', b:[] })).toThrow('b not provided.');
		expect(() => route.compile('q3', { b: ['b'] })).toThrow();

		expect(() => route.compile('none' as any)).toThrow();

		expect(route.compileOrNull('q2', { o:'o' })).toEqual('/q2/o')
		expect(route.compileOrNull('none')).toBeNull();

	})

});
